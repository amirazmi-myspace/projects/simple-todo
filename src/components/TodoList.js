export const TodoList = () => {

  const list = () => {
    const todoList = localStorage.getItem('todoArray').split(',')
    return todoList.map((todo, index) => (
      <div>{todo}</div>
    ))
  }

  return (
    <div>
      {list()}
    </div>
  )
}
