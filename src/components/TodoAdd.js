import React, { useState } from 'react'

export const TodoAdd = () => {
  const [addBtn, setAddBtn] = useState(false)
  const [input, setInput] = useState('')

  const submit = (e) => {
    e.preventDefault()
    const todoList = input.split('\n')
    const localTodoList = localStorage.getItem('todoArray') && localStorage.getItem('todoArray').split(',')
    const newTodoList = localTodoList ? [...localTodoList] : []
    todoList.forEach((todo) => newTodoList.push(todo))
    localStorage.setItem('todoArray', newTodoList)
    setInput('')
    document.location.reload()
  }

  const showTodoInput = () => {
    if (addBtn) {
      return (
        <form onSubmit={submit}>
          <label htmlFor='todoadd'>Add your todo lists here</label>
          <textarea id="todoadd" onChange={(e) => setInput(e.target.value)} placeholder={'Cooking\nEating\nCook for lunch\nSleeping at my home'} value={input} />
          <button type='submit'>Add Todos</button>
          <small>Please enter your next todo below the previous todo.</small>
        </form>
      )
    }
  }

  return (
    <div>
      <button type='button' onClick={() => setAddBtn(!addBtn)}>Add</button>
      {showTodoInput()}
    </div>
  )
}
