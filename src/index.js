import ReactDOM from 'react-dom'
import * as serviceWorkerRegistration from './serviceWorkerRegistration'
import './assets/main.css'
import { TodoAdd } from './components/TodoAdd'
import { TodoList } from './components/TodoList'

const App = () => {
  return (
    <div>
      <TodoAdd />
      <TodoList />
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
serviceWorkerRegistration.register()